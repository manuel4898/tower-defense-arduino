using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Castle : MonoBehaviour
{
    // Start is called before the first frame
    public Text text;

    public Text Final;
    public int hp = 15;

    //me falta cambiar el texto invisible en UI a derrota o victoria

    void Start()
    {
        text.text = hp.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (hp <= 0)
        {
            Final.text = "DERROTA";
            hp = 0;
        }

        text.text = hp.ToString();
    }
    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Enemy")
            hp--;
    }
}
