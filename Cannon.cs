﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using UnityEngine.UI;

public class Cannon : MonoBehaviour
{
    string[] puertos;
    SerialPort arduinoUno;
    public Dropdown lista_puertos;
    string puerto_seleccionado;
    bool conected = false;
    public GameObject cubo;
    public Text text;

    public GameObject bullet1;
    public GameObject bullet2;
    public GameObject bullet3;
    public GameObject bullet4;

    public GameObject target;

    int type = 1;

    int ammo1 = 24;
    int ammo2 = 24;
    int ammo3 = 24;
    int ammo4 = 24;

    int re1 = 2;
    int re2 = 2;
    int re3 = 2;
    int re4 = 2;

    private void Awake()
    {
        lista_puertos.options.Clear();
        puertos = SerialPort.GetPortNames();

        foreach (string port in puertos)
        {
            lista_puertos.options.Add(new Dropdown.OptionData() { text = port });
        }
        DropdownItemSelected(lista_puertos);
        lista_puertos.onValueChanged.AddListener(delegate { DropdownItemSelected(lista_puertos); });
        Debug.Log(puertos);
    }
    public void Conectar()
    {
        if (!conected)
        {
            arduinoUno = new SerialPort(puerto_seleccionado, 9600, Parity.None, 8, StopBits.One);
            arduinoUno.Open();
            Debug.Log("Conectado");

            conected = true;
            text.text = "Desconectar";
        }
        else
        {
            arduinoUno.Close();
            Debug.Log("Desconectado");

            conected = false;
            text.text = "Conectar";
        }
    }

    public void DropdownItemSelected(Dropdown lista)
    {
        int indice = lista.value;
        puerto_seleccionado = lista_puertos.options[indice].text;
        Debug.Log(puerto_seleccionado);
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (conected)
        {
            string resultado = arduinoUno.ReadLine();
            Debug.Log(resultado);
            // String of authors  
            string[] numbers = resultado.Split(';');

            int a_z = int.Parse(numbers[0]);
            int a_x = int.Parse(numbers[1]);
            int a_y = int.Parse(numbers[2]);


            cubo.transform.rotation = Quaternion.Euler(90 - a_x, -a_y, a_z);

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                arduinoUno.Write("1");
                type = 1;
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                arduinoUno.Write("2");
                type = 2;
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                arduinoUno.Write("3");
                type = 3;
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                arduinoUno.Write("4");
                type = 4;
            }
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                arduinoUno.Write("f");

                if (type == 1)
                {
                    if (ammo1 > 0)
                    {
                        Instantiate(bullet1, target.transform.position, cubo.transform.rotation).GetComponent<Rigidbody>().AddForce(target.transform.up * 2000, ForceMode.Acceleration);
                        ammo1--;
                    }
                    else
                    {
                        if (re1 > 0)
                        {
                            ammo1 = 24;
                            re1--;
                        }
                    }
                }
                else if (type == 2)
                {
                    if (ammo2 > 0)
                    {
                        Instantiate(bullet2, target.transform.position, cubo.transform.rotation).GetComponent<Rigidbody>().AddForce(target.transform.up * 1000, ForceMode.Acceleration);
                        ammo2--;
                    }
                    else
                    {
                        if (re2 > 0)
                        {
                            ammo2 = 24;
                            re2--;
                        }
                    }    
                }
                else if (type == 3)
                {
                    if (ammo3 > 0)
                    {
                        Instantiate(bullet3, target.transform.position, cubo.transform.rotation).GetComponent<Rigidbody>().AddForce(target.transform.up * 1500, ForceMode.Acceleration);
                        ammo3--;
                    }
                    else
                    {
                        if (re3 > 0)
                        {
                            ammo3 = 24;
                            re3--;
                        }
                    }
                }
                else if (type == 4)
                {
                    if (ammo4 > 0)
                    {
                        Instantiate(bullet4, target.transform.position, cubo.transform.rotation).GetComponent<Rigidbody>().AddForce(target.transform.up * 1500, ForceMode.Acceleration);
                        ammo4--;
                    }
                    else
                    {
                        if (re4 > 0)
                        {
                            ammo4 = 24;
                            re4--;
                        }
                    }
                }

            }
        
        }

    }

    public void LedSignalRed(bool rg)
    {
        if (rg)
            arduinoUno.Write("r");
        else
            arduinoUno.Write("s");
    }
    public void LedSignalGreen(bool rg)
    {
        if (rg)
            arduinoUno.Write("g");
        else
            arduinoUno.Write("h");
    }

}
