﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Start is called before the first frame update
    public float hits = 1;
    public float speed = 3;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        this.gameObject.transform.Translate(Vector3.left * Time.deltaTime * speed);

        if (hits <= 0)
        {
            Destroy(this.gameObject);
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Enemy")
        {
            hits--;
        }
    }
}
