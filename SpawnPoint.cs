﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    Vector3 newPos;
    float newY;

    // Start is called before the first frame update
    void Start()
    {
        newPos = this.gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        newY = Random.Range(1.0f,11.0f);
        newPos.y = newY;
        this.gameObject.transform.position = newPos;
    }
}
