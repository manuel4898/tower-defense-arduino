#include <Wire.h>
#include <LiquidCrystal.h>

LiquidCrystal lcd(5, 4, 11, 10, 9, 8);

const int MPU = 0x68; 
float AccX, AccY, AccZ;
float GyroX, GyroY, GyroZ;
float accAngleX, accAngleY, gyroAngleX, gyroAngleY, gyroAngleZ;
float roll, pitch, yaw;
float AccErrorX, AccErrorY, GyroErrorX, GyroErrorY, GyroErrorZ;
float elapsedTime, currentTime, previousTime;
int c = 0;

float reg_r;
float reg_p;
float reg_y;

String incoming;

int red = 6, green =7;
int sw = 3;
int sw_state;

int count1 = 2, count2 = 2, count3 = 2, count4 = 2;

int ammo1 =24, ammo2=24, ammo3=24, ammo4=24;

int current_ammo = 1;

bool shot = false;

String a;
void setup() {
  Serial.begin(9600);
  
  pinMode(sw, INPUT);

  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  digitalWrite(red, LOW);
  digitalWrite(green, LOW);

  lcd.begin(16,2);
  
  Wire.begin();                
  Wire.beginTransmission(MPU); 
  Wire.write(0x6B);            
  Wire.write(0x00);            
  Wire.endTransmission(true);  
  
  calculate_IMU_error();
  delay(20);
}
void loop() {

  sw_state = digitalRead(sw);
  
  for (int i = 0; i < 24; i++)
  {
    lcd.setCursor(i,0);
    lcd.print(" ");
    lcd.setCursor(i,1);
    lcd.print(" ");
  }
 
  Wire.beginTransmission(MPU);
  Wire.write(0x3B); 
  Wire.endTransmission(false);
  Wire.requestFrom(MPU, 6, true);


  AccX = (Wire.read() << 8 | Wire.read()) / 16384.0; 
  AccY = (Wire.read() << 8 | Wire.read()) / 16384.0; 
  AccZ = (Wire.read() << 8 | Wire.read()) / 16384.0; 

  accAngleX = (atan(AccY / sqrt(pow(AccX, 2) + pow(AccZ, 2))) * 180 / PI) + 6.63; 
  accAngleY = (atan(-1 * AccX / sqrt(pow(AccY, 2) + pow(AccZ, 2))) * 180 / PI) + 4.03; 
  
  previousTime = currentTime;        
  currentTime = millis();            
  elapsedTime = (currentTime - previousTime) / 1000; 
  Wire.beginTransmission(MPU);
  Wire.write(0x43); 
  Wire.endTransmission(false);
  Wire.requestFrom(MPU, 6, true); 
  GyroX = (Wire.read() << 8 | Wire.read()) / 131.0; 
  GyroY = (Wire.read() << 8 | Wire.read()) / 131.0;
  GyroZ = (Wire.read() << 8 | Wire.read()) / 131.0;
  
  GyroX = GyroX + 1.13; 
  GyroY = GyroY + 0.19; 
  GyroZ = GyroZ + 1.66; 
  
  gyroAngleX = gyroAngleX + GyroX * elapsedTime; 
  gyroAngleY = gyroAngleY + GyroY * elapsedTime;
  yaw =  yaw + GyroZ * elapsedTime;
  
  roll = 0.96 * gyroAngleX + 0.04 * accAngleX;
  pitch = 0.96 * gyroAngleY + 0.04 * accAngleY;

  reg_r+=0.05;
  reg_p+=0.08;
  //reg_y+=0.000002;

  roll+=reg_r;
  pitch-=reg_p;
  yaw-=0.14-reg_y;


  if (sw_state == HIGH)
  {
    reg_r = 0;
    reg_p = 0;
    gyroAngleX = 0;
    gyroAngleY = 0;
    roll=0;
    pitch=0;
    yaw=0;
  }

    
  a = "";
  a += int(roll);
  a += ";";
  a += int(pitch);
  a += ";";
  a += int(yaw);
  
  Serial.println(a);

  
  if (Serial.available() > 0) {
    
    incoming = Serial.read();
    if (incoming == "102") shot = true;

    if (incoming == "114")
    {
      digitalWrite(red, HIGH);
    }
    else if (incoming == "115")
    {
      digitalWrite(red, LOW);
    }
    if (incoming == "103")
    {
      digitalWrite(green, HIGH);
    }
    else if (incoming == "104")
    {
      digitalWrite(green, LOW);
    }
  
    
    if (incoming == "49") current_ammo = 1;
    else if (incoming == "50") current_ammo = 2;
    else if (incoming == "51") current_ammo = 3;
    else if (incoming == "52") current_ammo = 4;
    
  }

  lcd.setCursor(0,0);
  lcd.print("Tipo: ");
  lcd.print(current_ammo);

  if (current_ammo == 1){
    if(shot) ammo1 -=1;

    if(ammo1 < 1 and count1 > 0)
    {
      ammo1 = 24;
      count1 -= 1;
    }
    lcd.print("    R: ");
    lcd.print(count1);
    display_Bullets('I', ammo1);
  }
  else if (current_ammo == 2){
    if(shot) ammo2 -=1;

    if(ammo2 < 1 and count2 > 0)
    {
      ammo2 = 24;
      count2 -= 1;
    }
    lcd.print("    R: ");
    lcd.print(count2);
    display_Bullets('O', ammo2);
  }
  else if (current_ammo == 3){
    if(shot) ammo3 -=1;

    if(ammo3 < 1 and count3 > 0)
    {
      ammo3 = 24;
      count3 -= 1;
    }
    lcd.print("    R: ");
    lcd.print(count3);
    display_Bullets('Y', ammo3);
  }
  else if (current_ammo == 4){
    if(shot) ammo4 -=1;

    if(ammo4 < 1 and count4 > 0)
    {
      ammo4 = 24;
      count4 -= 1;
    }
    lcd.print("    R: ");
    lcd.print(count4);
    display_Bullets('W', ammo4);
  }
  
  shot = false;
  delay(100);
  
}

void display_Bullets(char ct, int ammount){

  for(int i = 0; i < ammount; i++)
  {
    lcd.setCursor(i,1);
    lcd.print(ct);
  }
}

void calculate_IMU_error() {
  
  while (c < 200) {
    Wire.beginTransmission(MPU);
    Wire.write(0x3B);
    Wire.endTransmission(false);
    Wire.requestFrom(MPU, 6, true);
    AccX = (Wire.read() << 8 | Wire.read()) / 16384.0 ;
    AccY = (Wire.read() << 8 | Wire.read()) / 16384.0 ;
    AccZ = (Wire.read() << 8 | Wire.read()) / 16384.0 ;
    
    AccErrorX = AccErrorX + ((atan((AccY) / sqrt(pow((AccX), 2) + pow((AccZ), 2))) * 180 / PI));
    AccErrorY = AccErrorY + ((atan(-1 * (AccX) / sqrt(pow((AccY), 2) + pow((AccZ), 2))) * 180 / PI));
    c++;
  }
  
  AccErrorX = AccErrorX / 200;
  AccErrorY = AccErrorY / 200;
  c = 0;
  
  while (c < 200) {
    Wire.beginTransmission(MPU);
    Wire.write(0x43);
    Wire.endTransmission(false);
    Wire.requestFrom(MPU, 6, true);
    GyroX = Wire.read() << 8 | Wire.read();
    GyroY = Wire.read() << 8 | Wire.read();
    GyroZ = Wire.read() << 8 | Wire.read();
    
    GyroErrorX = GyroErrorX + (GyroX / 131.0);
    GyroErrorY = GyroErrorY + (GyroY / 131.0);
    GyroErrorZ = GyroErrorZ + (GyroZ / 131.0);
    c++;
  }
  
  GyroErrorX = GyroErrorX / 200;
  GyroErrorY = GyroErrorY / 200;
  GyroErrorZ = GyroErrorZ / 200;

}
