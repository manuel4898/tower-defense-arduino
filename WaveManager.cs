﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveManager : MonoBehaviour
{
    public List<WaveObject> waves = new List<WaveObject>();
    public bool isWaitingForNextWave;
    public bool wavesFinish;
    public int currentWave;
    public Transform initPosition;

    public Enemy enemy;

    public Cannon player;

    bool ledOnce = true;

    private void Start()
    {
        StartCoroutine(PreArduino());
    }
    private IEnumerator PreArduino()
    {
        yield return new WaitForSeconds(5);
        StartCoroutine(ProcesWave());
    }
    private void Update()
    {
        checkCounterForNextWave();
    }
    private void checkCounterForNextWave()
    {
        bool allDead = true;

        for (int i = 0; i < waves[currentWave].enemys.Count; i++)
        {
            if (waves[currentWave].enemys[i] != null) allDead = false;
        }

        if (allDead && ledOnce)
        {
            isWaitingForNextWave = true;
            player.LedSignalRed(false);
            player.LedSignalGreen(true);
            ledOnce = false;
        }

        if (isWaitingForNextWave && !wavesFinish)
        {
            waves[currentWave].counterToNextWave -= 1 * Time.deltaTime;

            if (waves[currentWave].counterToNextWave <= 0)
            {
                ChangeWave();
                Debug.Log("Set Next Wave");
            }
        }
    }

    public void ChangeWave()
    {
        if (wavesFinish)
            return;
        currentWave++;
        player.LedSignalGreen(false);
        ledOnce = true;
        StartCoroutine(ProcesWave());
    }

    private IEnumerator ProcesWave()
    {
        if (wavesFinish)        
            yield break;

        player.LedSignalRed(true);
        isWaitingForNextWave = false;
        waves[currentWave].counterToNextWave = waves[currentWave].timeForNextWave;
        for (int i = 0; i < waves[currentWave].enemys.Count; i++)
        {
            var enemyGo = Instantiate(enemy, initPosition.position, initPosition.rotation);

            waves[currentWave].enemys[i] = enemyGo;

            yield return new WaitForSeconds(waves[currentWave].timerPerCreation);
        }

        if (currentWave >= waves.Count-1)
        {
            Debug.Log("Nivel terminado");
            wavesFinish = true;
        }

    }

}

[System.Serializable]
public class WaveObject
{
    public float timerPerCreation = 1.5f;
    public float timeForNextWave = 60;
    public float counterToNextWave = 60;
    public List<Enemy> enemys = new List<Enemy>();

}
